"use strict"

const itemInfoArray = [
    {
        description: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem,
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.
        Morbi pulvinar odio eget aliquam facilisis.`,
        person: ['Hasan', 'Al'],
        ocupation: 'UX Designer',
        image: './img/info_people/Hasan_Al.jpg',
        alt: 'Hasan Al',
    },
    {
        description: `People have questions of life and death - How to display a thumbnail 
        on the main page in WordPress? But so that I do not create a thumbnail in the admin 
        itself. That is, to have one large picture on the page I am creating, without a 
        thumbnail, and a thumbnail is already displayed on the main page???.`,
        person: ['Kamila', 'Lost'],
        ocupation: 'QA Engineer',
        image: './img/info_people/Kamila_Lost.jpg',
        alt: 'Kamila Lost',
    },
    {
        description: `Wordpress is clear, simple and understandable. If I managed to make 
        a site on Wordpress, then for others it will also not be too difficult. And to be honest, 
        I didn't even expect that thanks to Wordpress you can easily start your own business. 
        I want to share my story))) I have been fond of various crafts since childhood and 
        I have accumulated a huge amount of them, my girlfriend advised me to sell them....`,
        person: ['Vasilisa', 'Kononets'],
        ocupation: 'UX Designer',
        image: './img/info_people/Vasilisa_Kononets.jpg',
        alt: 'Vasilisa Kononets',
    },
    {
        description: `Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem,
        non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.
        Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.
        Morbi pulvinar odio eget aliquam facilisis.`,
        person: ['Victor', 'But'],
        ocupation: 'Developer',
        image: './img/info_people/Victor_But.jpg',
        alt: 'Victor But',
    },
]

const sectionSayAbout = document.querySelector('.section-say-about');
const itemInfo = document.createElement('div');
const itemInfoHTML = itemInfoArray.map((el, index) => `
<div data-num= ${index+1} class="wrapper-say-about">                  
                            <p class="info-description">${el.description}</p>
                <div class="info">
                    <p class="info-name">${el.person[0]} ${el.person[1]}</p>
                    <p class="info-ocupation">${el.ocupation}</p>
                </div>
                <img class="info-photo" src=${el.image} alt=${el.alt}>
              
            </div>
`)
sectionSayAbout.insertAdjacentHTML('afterbegin', itemInfoHTML.slice(0, 1).join(''));
 
const itemRoundabout = document.querySelectorAll('.roundabout li');
const imgRoundabout = document.querySelectorAll('.roundabout img');
const btnPre = document.querySelector('.btn-pre');
const btnNext = document.querySelector('.btn-next');

let activeItem;
itemRoundabout.forEach(el => {
   
    el.addEventListener('click', e => {
        imgRoundabout.forEach(element => {
           element.closest('li').classList.remove('up'); 
           element.closest('li').classList.add('no-up'); 

        })
        activeItem = document.querySelector('.wrapper-say-about');
        
        itemInfoArray.forEach((elem, index) => {
            activeItem.remove()
            if (e.target.alt === elem.alt) {
                e.target.closest('li').classList.add('up');
                sectionSayAbout.insertAdjacentHTML('afterbegin', itemInfoHTML.slice(index, index + 1).join(''))
            }
        })
    })
})



btnNext.addEventListener('click', e => {
    activeItem = document.querySelector('.wrapper-say-about');
    itemInfoArray.forEach((el, index) => {
        if (activeItem.dataset.num >= itemInfoArray.length) {
            activeItem.dataset.num = 0;
            index -= 1;
        }
        activeItem.remove()
        if ((index + 1) == activeItem.dataset.num) {
            sectionSayAbout.insertAdjacentHTML('afterbegin', itemInfoHTML.slice(index + 1, index + 2).join(''))
        }
        itemRoundabout.forEach((elem, index) => {
            elem.classList.remove('up');
            elem.classList.add('no-up');
            if (index == activeItem.dataset.num) {
                elem.classList.add('up')
            }
        })

    })
}) 

btnPre.addEventListener('click', e => {
    activeItem = document.querySelector('.wrapper-say-about');
    itemInfoArray.forEach((el, index) => {
        if (activeItem.dataset.num <= 1) {
            activeItem.dataset.num = 5;
        }
        activeItem.remove()
        if ((index + 2) == activeItem.dataset.num) {
            sectionSayAbout.insertAdjacentHTML('afterbegin', itemInfoHTML.slice(index, index+1).join(''))
        }
        itemRoundabout.forEach((elem, index) => {
            elem.classList.remove('up');
            elem.classList.add('no-up');
            if (index == (activeItem.dataset.num-2)) {
                elem.classList.add('up')
            }
        })

    })
}) 

