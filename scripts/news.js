"use strict"

const newsArray = [
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_1.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_2.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_3.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_4.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_5.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_6.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_7.jpg',
        date: [12, 'feb'],
    },
    {
        link: 'https://creativeeurope.in.ua/posts',
        image: './img/news/Layer_8.jpg',
        date: [12, 'feb'],
    },
]

const newsContainer = document.querySelector('.grid-container-news');
const newsCard = document.createElement('a');
const newsCardHTML = newsArray.map(el =>  `
<a class="news-wrapper" href=${el.link}>
                        <img src=${el.image} alt="">
                        <div class="news-comment">
                        <p class="news-description">Amazing Blog Post</p>
                        <span>By admin |</span>
                        <span>2 comment</span>
                        </div>
                    <div class="date">${el.date[0]}<br>${el.date[1]}</div>
                    </a>
`)

newsContainer.insertAdjacentHTML('beforeend', newsCardHTML.join(''));

