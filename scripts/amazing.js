"use strict"


// Фільтр в секції "Our Amazing Work" 
const tabAmazingWork = document.querySelectorAll('.amazing-work-item');
const ulAmazingWork = document.querySelector('.tabs-amazing-work')

const flexContainerAmazing = document.querySelector('.flex-container-amazing')

const cardArray = [
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design0.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design1.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design2.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design0.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design1.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design2.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page0.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page1.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page2.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress0.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress1.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress2.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design3.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design4.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design5.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design3.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design4.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: 'img/web/web_design5.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page3.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: 'img/landing/landing_page4.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page5.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress3.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress4.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress5.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design6.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design7.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design8.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design6.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design7.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design8.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page6.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page7.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page8.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress6.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress7.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress8.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design9.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: 'img/graphic/graphic_design10.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Graphic',
        image: './img/graphic/graphic_design11.jpg',
        alt: 'Graphic Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design9.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design10.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Web',
        image: './img/web/web_design11.jpg',
        alt: 'Web Design',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page9.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page10.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Landing',
        image: './img/landing/landing_page11.jpg',
        alt: 'Landing Page',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress9.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress10.jpg',
        alt: 'Wordpress',
    },
    {
        dataset: 'Wordpress',
        image: './img/wordpress/wordpress11.jpg',
        alt: 'Wordpress',
    },
]

const cardsHTML = cardArray.map(el => `
    <div class="card"  data-alt=${el.dataset}>
                    <img class="front-card" src=${el.image} alt=${el.alt}>
                     <div class="back-card">
                         <span>creative design</span>
                        <p>${el.alt}</p>
                     </div>
                 </div>
    `);

    let quantity = 12;

    flexContainerAmazing.insertAdjacentHTML('beforeend', cardsHTML.slice(0, quantity).join(''));

    const load = document.createElement('div');
    load.innerHTML = `
    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    `
    const btnContainer = document.querySelector('.js-btn-hidden');
    const btnLoadMore = document.querySelector('.btn-plus');
    const loadContainer = document.querySelector('.load');
    loadContainer.appendChild(load);
    const loadSpiner = document.querySelector('.lds-spinner');
    
    let cards = Object.values(document.querySelectorAll('.card'));

    btnLoadMore.addEventListener('click', e => {  
    loadSpiner.classList.toggle('active-preloader');
    setTimeout(() => {
        loadSpiner.classList.toggle('active-preloader');
        flexContainerAmazing.insertAdjacentHTML('beforeend', cardsHTML.slice(quantity, quantity + 12).join(''));
        quantity += 12;
        cards = Object.values(document.querySelectorAll('.card'));
        if (quantity >= 36) {
            btnContainer.style.visibility = 'hidden'
        }
    }, 2000)
})
   
    tabAmazingWork.forEach(el => {
    
        ulAmazingWork.addEventListener('click', (e => {
            el.style.border = '1px solid  #EDEFEF';
            el.style.color = '#717171';
            if (el === e.target) {
                el.style.border = '1px solid #18CFAB';
                el.style.color = '#18CFAB'
            }
        }))
    
        el.addEventListener('click', e => {
            cards.forEach(elem => {
                elem.style.display = 'none';
                if (elem.dataset.alt === el.dataset.alt) { 
                    elem.style.display = 'inline-block';
                } else if (el.innerText === 'All') {
                    elem.style.display = 'inline-block';
                }
            })
    
        })
    })
    
    
    
  






