"use strict"

// Перебір вкладок в секції "Our Services"

const listContentServices = document.querySelectorAll(".services-list-content li");
const listService = document.querySelectorAll('.services-list-item')
const ulListService = document.querySelector('.services-list')

listService.forEach(el => {
    
    ulListService.addEventListener('click', (e => {
        el.classList.remove('active')
               if (el === e.target) { 
            el.classList.add('active')
        }  
        listContentServices.forEach(el => {
            el.style.display = 'none';
            if (el.dataset.tab === e.target.innerText) {
                el.style.display = 'flex';
            }
        })

    }))
    ulListService.addEventListener('mouseover', e => {
        el.style.border = '1px solid  #EDEFEF';
        if (el === e.target) {
            el.style.border = '1px solid #18CFAB';
        }
    })
})

