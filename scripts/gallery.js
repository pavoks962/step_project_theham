"use strict"

const galleryContainer = document.querySelector('#gallery-container');

const galleryLoader = imagesLoaded(galleryContainer);


function getGallery(e) {
    imagesLoaded(galleryContainer, function () {
      new Masonry(galleryContainer, {
        itemSelector: '.item-masonry',
        columnWidth: '.sizer1',
        horizontalOrder: true,
        percentPosition: true,
        gutter: 20,
      });
    });
  }
  galleryLoader.on('always', getGallery);


const itemMasonry =document.querySelectorAll('.js-mouse');
const darkDiv = document.createElement('div');
darkDiv.innerHTML = `
<div class="dark">
                        <div class="back-item-gallery"></div>
                        <div class="back-item-icons">
                            <div class="search"></div>
                            <div class="size"></div>
                        </div>
                    </div>
`

itemMasonry.forEach(el => {
    
    el.addEventListener('mouseover', e => {
      setTimeout(() => {
        el.append(darkDiv);
        const dark = document.querySelector('.dark');
        dark.style.display = 'flex';
      }, 300)
    })
})



const loadG = document.createElement('div');
    loadG.innerHTML = `
    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
    `
    const loadContainerG = document.querySelector('.load-gallery');
    loadContainerG.appendChild(loadG);
const btnGallery = document.querySelector('.btn-gallery');
const loadSpinerGallery = document.querySelector('.load-gallery .lds-spinner');

btnGallery.addEventListener('click', e => {  
    loadSpinerGallery.classList.toggle('active-preloader');
 setTimeout(() => {
    loadSpinerGallery.classList.toggle('active-preloader');
 }, 2000)
})




